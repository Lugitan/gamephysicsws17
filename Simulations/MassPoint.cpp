#include "masspoint.h"
using namespace GamePhysics;

MassPoint::MassPoint(Vec3 pos, Vec3 vel, bool isFix, float m)
{
	position = pos;
	velocity = vel;
	isFixed = isFix;
	mass = m; 
}

MassPoint::~MassPoint() {}