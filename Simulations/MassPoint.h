#pragma once
#include "util/vectorbase.h"
using namespace GamePhysics;

class MassPoint 
{
	MassPoint(Vec3 position, Vec3 velocity, bool isFixed, float mass);
	~MassPoint();
	
	Vec3 position;
	Vec3 velocity;
	float mass;
	bool isFixed;
	float damping;
	Vec3 force;
};