#include "MassSpringSystemSimulator.h"

MassSpringSystemSimulator::MassSpringSystemSimulator()
{
	m_iTestCase = 0;
	m_fDamping = 0.0f;
	m_fFriction = 0.0f;
	m_fMass = 0.0f;
	m_fStiffness = 0.0f;
	m_fbBoxSize = 1.0f;
	m_fBounceBack = 0.0f;
	m_fSphereSize = 0.02f;

	m_bApplyGravity = false;
	m_bBoundingBox = false;
	m_bAddFriction = false;

	m_iAlreadySet = -1;
	m_iIntegrator = 0;

	m_externalForce = Vec3();
	m_vGravity = Vec3(0, 0, 0);
	m_vLineColor = Vec3(255, 255, 0);
	m_vSphereScale = Vec3(1.0f, 1.0f, 1.0f);
}

void MassSpringSystemSimulator::initializeSceneObjects()
{
	m_vlMassPoints.clear();
	m_vlSprings.clear();

	m_fSphereSize = 0.02f;

	switch (m_iTestCase)
	{
	case 0:
	{
		if (m_iAlreadySet != 0)
		{
			m_bApplyGravity = false;
			m_bBoundingBox = false;
			setMass(10);
			setStiffness(40);
			setDampingFactor(0);

			m_iAlreadySet = 0;
		}

		int mp1 = addMassPoint(Vec3(0, 0, 0), Vec3(0, 0, 0), false);
		int mp2 = addMassPoint(Vec3(0, 2, 0), Vec3(0, 0, 0), false);

		addSpring(mp1, mp2, 1.0f);

		cout << "Test case Begin with:\n";
		for (size_t i = 0; i < m_vlMassPoints.size(); i++)
			printPoint((int)i);
		switch (m_iIntegrator)
		{
		case EULER:
			useEulerMethod(0.05f);
			cout << "After an Euler step:\n";
			break;
		case LEAPFROG:
			useLeapFrogMethod(0.05f);
			cout << "After an LeapFrog step:\n";
			break;
		case MIDPOINT:
			useMidpointMethod(0.05f);
			cout << "After an Midpoint step:\n";
			break;
		default:
			useEulerMethod(0.05f);
			cout << "After an Default step:\n";
			break;

		}
		for (size_t i = 0; i < m_vlMassPoints.size(); i++)
			printPoint(i);
	}
	break;
	case 1:
	{
		if (m_iAlreadySet != 1)
		{
			m_bApplyGravity = true;
			m_bBoundingBox = false;
			m_fBounceBack = 0.2f;
			m_fbBoxSize = 1.0f;
			setMass(0.3f);
			setStiffness(30);
			setDampingFactor(0.2f);

			m_iAlreadySet = 1;
		}

		int mp1 = addMassPoint(Vec3(0.3f, 0, 0), Vec3(0, 1, 0), false);
		int mp2 = addMassPoint(Vec3(0, 0.2f, 0), Vec3(0, 0, 0), true);
		int mp3 = addMassPoint(Vec3(-0.1f, 0, 0), Vec3(0, 0, 0), false);

		newSpring(mp1, mp2);
		newSpring(mp1, mp3);
	}
	break;
	case 2:
	{
		if (m_iAlreadySet != 2)
		{
			m_bApplyGravity = true;
			m_bBoundingBox = true;
			m_fbBoxSize = 2.0f;
			m_fBounceBack = 0.2f;
			setMass(0.3f);
			setStiffness(30);
			setDampingFactor(0.25f);

			m_iAlreadySet = 2;
		}

		int mp1 = addMassPoint(Vec3(0, 0, 0), Vec3(0, 0, 0), false);
		int mp2 = addMassPoint(Vec3(0, 0.5, 0), Vec3(0, 0, 0), false);
		int mp3 = addMassPoint(Vec3(0.5, 0, 0), Vec3(0, 0, 0), false);
		int mp4 = addMassPoint(Vec3(0.5, 0.5, 0), Vec3(0, 0, 0), false);

		int mp5 = addMassPoint(Vec3(0, 0, -0.5), Vec3(0, 0, 0), false);
		int mp6 = addMassPoint(Vec3(0, 0.5, -0.5), Vec3(0, 0, 0), false);
		int mp7 = addMassPoint(Vec3(0.5, 0, -0.5), Vec3(0, 0, 0), false);
		int mp8 = addMassPoint(Vec3(0.5, 0.5, -0.5), Vec3(0, 0, 0), false);

		//front facing square
		// 2 --- 4
		// |     |
		// 1 --- 3
		newSpring(mp1, mp2);
		newSpring(mp1, mp3);
		newSpring(mp2, mp4);
		newSpring(mp3, mp4);

		//back facing square
		// 6 --- 8
		// |     |
		// 5 --- 7
		newSpring(mp5, mp6);
		newSpring(mp5, mp7);
		newSpring(mp6, mp8);
		newSpring(mp7, mp8);

		//connection between those squares
		// 2 --- 6			4 --- 8
		// |     |			|     |
		// 1 --- 5			3 --- 7
		newSpring(mp1, mp5);
		newSpring(mp2, mp6);
		newSpring(mp3, mp7);
		newSpring(mp4, mp8);

		//front cross
		// 2   4
		//   X
		// 1   3
		newSpring(mp1, mp4);
		newSpring(mp2, mp3);
			
		//back cross
		// 6   8
		//   X
		// 5   7
		newSpring(mp5, mp8);
		newSpring(mp6, mp7);

		//left cross
		// 2   6
		//   X
		// 1   5
		newSpring(mp1, mp6);
		newSpring(mp2, mp5);
			
		//right cross
		// 4   8
		//   X
		// 3   7
		newSpring(mp3, mp8);
		newSpring(mp4, mp7);

		//top cross
		// 6   8
		//   X
		// 2   4
		newSpring(mp2, mp8);
		newSpring(mp6, mp4);

		//bottom cross
		// 5   7
		//   X
		// 1   3
		newSpring(mp1, mp7);
		newSpring(mp5, mp3);

	}
	break;
	case 3:
	{
		if (m_iAlreadySet != 3)
		{
			m_bApplyGravity = true;
			m_bBoundingBox = false;
			m_fbBoxSize = 2.0f;
			m_fBounceBack = 0.2f;
			setMass(0.1f);
			setStiffness(100);
			setDampingFactor(0.2f);

			m_iAlreadySet = 3;
			m_iIntegrator = LEAPFROG;	
		}
		m_fSphereSize = 0.01f;

		int numberOfSpheres = 10;
		for (int x = 0; x <= numberOfSpheres; x++)
		{
			for (int y = numberOfSpheres; y >= 0; y--)
			{
				if (y == numberOfSpheres)
				{
					addMassPoint(Vec3(x*0.08f, y*0.08f, 0), Vec3(0, 0, 0), true);
				}
				else
				{
					addMassPoint(Vec3(x*0.08f, y*0.08f, 0), Vec3(0, 0, 0), false);
				}
			}
		}

		for (int i = 0; i < m_vlMassPoints.size() - 1; i++)
		{
			if (m_vlMassPoints[i].position.y != 0)
			{
				newSpring(i, i + 1);
			}
			if(i + numberOfSpheres < m_vlMassPoints.size()-1)
			{
				newSpring(i, i + (numberOfSpheres+1));
			}
		}
	}
	break;
	}
}

const char * MassSpringSystemSimulator::getTestCasesStr()
{
	return "Output Test, Spring Test, Cube Test, Cloth Test";
}

const char * MassSpringSystemSimulator::getIntegrationMethodStr()
{
	return "Euler, Leap-Frog, Midpoint";
}

void MassSpringSystemSimulator::initUI(DrawingUtilitiesClass * DUC)
{
	this->DUC = DUC;
	TwType TW_TYPE_Integration = TwDefineEnumFromString("Integration", getIntegrationMethodStr());

	// define TweakBar for all Test Scenes 
	TwAddVarRW(DUC->g_pTweakBar, "Gravity", TW_TYPE_BOOLCPP, &m_bApplyGravity, "");
	TwAddVarRW(DUC->g_pTweakBar, "BoundingBox", TW_TYPE_BOOLCPP, &m_bBoundingBox, "");
	TwAddVarRW(DUC->g_pTweakBar, "Size BoundingBox", TW_TYPE_FLOAT, &m_fbBoxSize, "step=0.1 min=0.0001");
	TwAddVarRW(DUC->g_pTweakBar, "BounceBack", TW_TYPE_FLOAT, &m_fBounceBack, "step=0.1 min=0.0001 max=0.9");
	TwAddVarRW(DUC->g_pTweakBar, "Integration", TW_TYPE_Integration, &m_iIntegrator, "");
	TwAddVarRW(DUC->g_pTweakBar, "Mass", TW_TYPE_FLOAT, &m_fMass, "step=0.1 min=0.0001");
	TwAddVarRW(DUC->g_pTweakBar, "Damping", TW_TYPE_FLOAT, &m_fDamping, "step=0.1 min=0.0001 max=0.9");
	switch (m_iTestCase)
	{
	case 0:
		// Remove Options not needed for this perticular Scene
		TwRemoveVar(DUC->g_pTweakBar, "BoundingBox");
		TwRemoveVar(DUC->g_pTweakBar, "Size BoundingBox");
		TwRemoveVar(DUC->g_pTweakBar, "BounceBack");
		TwRemoveVar(DUC->g_pTweakBar, "Damping");
		break;
	default:
		break;
	}
}

void MassSpringSystemSimulator::reset()
{
	m_mouse.x = m_mouse.y = 0;
	m_trackmouse.x = m_trackmouse.y = 0;
	m_oldtrackmouse.x = m_oldtrackmouse.y = 0;
}

void MassSpringSystemSimulator::drawFrame(ID3D11DeviceContext * pd3dImmediateContext)
{
	for (int i = 0; i < m_vlMassPoints.size(); i++)
	{
		DUC->drawSphere(m_vlMassPoints[i].position, m_vSphereScale * m_fSphereSize);
	}
	if (!m_vlSprings.empty())
	{
		for (int i = 0; i < m_vlSprings.size(); i++)
		{
			DUC->beginLine();
			DUC->drawLine(m_vlSprings[i].massPoint1->position, m_vLineColor, m_vlSprings[i].massPoint2->position, m_vLineColor);
			DUC->endLine();
		}
	}
}

void MassSpringSystemSimulator::notifyCaseChanged(int testCase) 
{
	m_iTestCase = testCase;

	switch (m_iTestCase)
	{
	case 0:
		cout << "Exercise 1 Output Test!\n";
		initializeSceneObjects();
		break;
	case 1:
		cout << "Exercise 1 Spring Test\n";
		initializeSceneObjects();
		break;
	case 2:
		cout << "Exercise 1 Cube Test\n";
		initializeSceneObjects();
		break;
	case 3:
		cout << "Exercise 1 Cloth Test\n";
		initializeSceneObjects();
		break;
	}
}

void MassSpringSystemSimulator::onClick(int x, int y)
{
	m_trackmouse.x = x;
	m_trackmouse.y = y;
}

void MassSpringSystemSimulator::onMouse(int x, int y)
{
	m_oldtrackmouse.x = x;
	m_oldtrackmouse.y = y;
	m_trackmouse.x = x;
	m_trackmouse.y = y;
}

void MassSpringSystemSimulator::externalForcesCalculations(float timeElapsed)
{
	Point2D mouseDiff;
	mouseDiff.x = m_trackmouse.x - m_oldtrackmouse.x;
	mouseDiff.y = m_trackmouse.y - m_oldtrackmouse.y;
	Vec3 gravity = Vec3(0, 0, 0);

	Vec3 appliedForce = Vec3(0, 0, 0);

	if (mouseDiff.x != 0 || mouseDiff.y != 0)
	{
		Mat4 worldViewInv = Mat4(DUC->g_camera.GetWorldMatrix() * DUC->g_camera.GetViewMatrix());
		//Mat4 viewMat = Mat4(DUC->g_camera.GetViewMatrix);
		worldViewInv = worldViewInv.inverse();
		Vec3 inputView = Vec3((float)mouseDiff.x, (float)-mouseDiff.y, 0);
		Vec3 inputWorld = worldViewInv.transformVectorNormal(inputView);
		// find a proper scale!
		float inputScale = 0.5f;
		appliedForce = appliedForce + (inputWorld * inputScale);
		mouseDiff.x = mouseDiff.y = 0; 
	}
	appliedForce -= appliedForce * 5.0f * timeElapsed;
	applyExternalForce(appliedForce);
}

void MassSpringSystemSimulator::simulateTimestep(float timeStep)
{
	switch (m_iIntegrator)
	{
	case EULER:
		useEulerMethod(timeStep);
		break;
	case LEAPFROG:
		useLeapFrogMethod(timeStep);
		break;
	case MIDPOINT:
		useMidpointMethod(timeStep);
		break;
	default:
		useEulerMethod(timeStep);
		break;
	}
	applyExternalForce(m_externalForce);
	checkFloorCollision();
}

void MassSpringSystemSimulator::setMass(float mass)
{
	m_fMass = mass;
}

void MassSpringSystemSimulator::setStiffness(float stiffness)
{
	m_fStiffness = stiffness;
}

void MassSpringSystemSimulator::setDampingFactor(float damping)
{
	m_fDamping = damping;
}

int MassSpringSystemSimulator::addMassPoint(Vec3 position, Vec3 Velocity, bool isFixed)
{
	MassPoint mp;
	mp.position = position;
	mp.velocity = Velocity;
	mp.mass = m_fMass;
	mp.isFixed = isFixed;

	m_vlMassPoints.push_back(mp);

	return m_vlMassPoints.size() - 1;
}

void MassSpringSystemSimulator::newSpring(int masspoint1, int masspoint2)
{
	float length = norm(m_vlMassPoints[masspoint1].position - m_vlMassPoints[masspoint2].position);
	addSpring(masspoint1, masspoint2, length);
}
void MassSpringSystemSimulator::addSpring(int masspoint1, int masspoint2, float initialLength)
{
	Spring s;
	s.massPoint1 = &m_vlMassPoints[masspoint1];
	s.massPoint2 = &m_vlMassPoints[masspoint2];
	s.stiffness = m_fStiffness;
	s.currentlength = s.massPoint1->position - s.massPoint2->position;
	s.initialLength = initialLength;
	m_vlSprings.push_back(s);
}

int MassSpringSystemSimulator::getNumberOfMassPoints()
{
	return m_vlMassPoints.size();
}

int MassSpringSystemSimulator::getNumberOfSprings()
{
	return m_vlSprings.size();
}

Vec3 MassSpringSystemSimulator::getPositionOfMassPoint(int index)
{
	return m_vlMassPoints.at(index).position;
}

Vec3 MassSpringSystemSimulator::getVelocityOfMassPoint(int index)
{
	return m_vlMassPoints.at(index).velocity;
}

void MassSpringSystemSimulator::applyExternalForce(Vec3 force)
{
	//apply Force to all Masspoints (gravity, user interaction, etc.)
	if (m_bApplyGravity) m_vGravity = Vec3(0, -9.81f, 0);
	else m_vGravity = Vec3(0, 0, 0);
	m_externalForce = force;
}

void MassSpringSystemSimulator::checkFloorCollision()
{
	if (m_bBoundingBox)
	{
		for (auto it = m_vlMassPoints.begin(); it != m_vlMassPoints.end(); it++)
		{
			if (!it->isFixed)
			{
				Vec3 pos = it->position;
				Vec3 vel = it->velocity;

				for (int f = 0; f < 6; f++)
				{
					float sign = (f % 2 == 0) ? -1.0f : 1.0f;
					if (sign * pos.value[f / 2] < -0.5f * m_fbBoxSize)
					{
						pos.value[f / 2] = sign * -0.5f * m_fbBoxSize;
						vel.value[f / 2] = vel.value[f / 2] * -m_fBounceBack;
						m_bAddFriction = true;
					}
					else
					{
						m_bAddFriction = false;
					}
				}
				it->position = pos;
				it->velocity = vel;
			}
		}
	}
}

int counter = 0;
void MassSpringSystemSimulator::calculateForce()
{
	// Equation of Motion: ma = (F_int + F_ext) - (F_damp)
	// F_ext = mg
	// F_int = -k * (l - L) * ((x1 - x2)/l)
	// F_damp = -� * v

	for (auto it = m_vlMassPoints.begin(); it != m_vlMassPoints.end(); it++)
	{
		// calculate external Forces
		it->force = (m_externalForce + m_vGravity) * m_fMass;
	}

	for (auto it = m_vlSprings.begin(); it != m_vlSprings.end(); it++)
	{
		//calculate internal Force
		it->currentlength = it->massPoint1->position - it->massPoint2->position;
		float l = norm(it->currentlength);
		float L = it->initialLength;
		float k = m_fStiffness;

		Vec3 F = (-k * (l - L) * (it->currentlength/ l));

		it->massPoint1->force += F;
		it->massPoint2->force -= F;
	}

	for (auto it = m_vlMassPoints.begin(); it != m_vlMassPoints.end(); it++)
	{
		//take damping into account
		/*if (!m_bAddFriction)
		{
			it->force += it->velocity * -m_fDamping;
		}
		else
		{
			it->force += it->velocity * -(m_fFriction + m_fDamping);
		}*/
		it->force += it->velocity * -m_fDamping;
	}
	counter++;
}

void MassSpringSystemSimulator::updatePosition(float timeStep)
{
	for (auto it = m_vlMassPoints.begin(); it != m_vlMassPoints.end(); it++)
	{
		if (!it->isFixed)
		{
			it->position += it->velocity * timeStep;
		}
	}
}

void MassSpringSystemSimulator::updateVelocity(float timeStep)
{
	for (auto it = m_vlSprings.begin(); it != m_vlSprings.end(); it++)
	{
		it->massPoint1->velocity += it->massPoint1->force * (timeStep/m_fMass);
		it->massPoint2->velocity += it->massPoint2->force * (timeStep/m_fMass);
	}

	//Reset forces
	for (auto it = m_vlMassPoints.begin(); it != m_vlMassPoints.end(); it++)
	{
		it->force = Vec3(0, 0, 0);
	}
}

void MassSpringSystemSimulator::useEulerMethod(float timeStep)
{
	calculateForce();
	updatePosition(timeStep);	
	updateVelocity(timeStep);
}

void MassSpringSystemSimulator::useMidpointMethod(float timeStep)
{
	//Save old MassPoint to use it in second Midpoint step
	std::vector<MassPoint> old_MassPoints = m_vlMassPoints;

	//calculate 
	calculateForce();
	updatePosition(timeStep / 2);
	updateVelocity(timeStep / 2);

	calculateForce();
	//Restore Positions to calculate full Midpoint step with before calculated half step
	for (size_t i = 0; i < m_vlMassPoints.size(); i++)
	{
		m_vlMassPoints[i].position = old_MassPoints[i].position;
	}
	updatePosition(timeStep);

	//Restore Velocities for the same reason
	for (size_t i = 0; i < m_vlMassPoints.size(); i++)
	{
		m_vlMassPoints[i].velocity = old_MassPoints[i].velocity;
	}
	updateVelocity(timeStep);
}

void MassSpringSystemSimulator::useLeapFrogMethod(float timeStep)
{
	calculateForce();
	updateVelocity(timeStep);
	updatePosition(timeStep);
}

void MassSpringSystemSimulator::printPoint(size_t p)
{
	cout << "point: " << p << " => velocity: (" << m_vlMassPoints[p].velocity.x << ", " << m_vlMassPoints[p].velocity.y << ", " << m_vlMassPoints[p].velocity.z
		<< "), position (" << m_vlMassPoints[p].position.x << ", " << m_vlMassPoints[p].position.y << ", " << m_vlMassPoints[p].position.z << ") \n";
}