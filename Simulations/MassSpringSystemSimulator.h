#ifndef MASSSPRINGSYSTEMSIMULATOR_h
#define MASSSPRINGSYSTEMSIMULATOR_h
#include "Simulator.h"
#include "MassPoint.h"
#include "Spring.h"

// Do Not Change
#define EULER 0
#define LEAPFROG 1
#define MIDPOINT 2
// Do Not Change


class MassSpringSystemSimulator:public Simulator{
public:
	// Construtors
	MassSpringSystemSimulator();
	
	// UI Functions
	const char * getTestCasesStr();
	void initUI(DrawingUtilitiesClass * DUC);
	void reset();
	void drawFrame(ID3D11DeviceContext* pd3dImmediateContext);
	void notifyCaseChanged(int testCase);
	void externalForcesCalculations(float timeElapsed);
	void simulateTimestep(float timeStep);
	void onClick(int x, int y);
	void onMouse(int x, int y);

	// Specific Functions
	void setMass(float mass);
	void setStiffness(float stiffness);
	void setDampingFactor(float damping);
	int addMassPoint(Vec3 position, Vec3 Velocity, bool isFixed);
	void newSpring(int massPoint1, int massPoint2);
	void addSpring(int masspoint1, int masspoint2, float initialLength);
	int getNumberOfMassPoints();
	int getNumberOfSprings();
	Vec3 getPositionOfMassPoint(int index);
	Vec3 getVelocityOfMassPoint(int index);
	void applyExternalForce(Vec3 force);

	// Individual Functions
	const char * getIntegrationMethodStr();
	void useEulerMethod(float timeStep);
	void useMidpointMethod(float timeStep);
	void useLeapFrogMethod(float timeStep);
	void initializeSceneObjects();
	void checkFloorCollision();
	void printPoint(size_t i);
	void updatePosition(float timeStep);
	void updateVelocity(float timeStep);
	void calculateForce();
	
	// Do Not Change
	void setIntegrator(int integrator) {
		m_iIntegrator = integrator;
	}

private:
	// Data Attributes
	float m_fMass;
	float m_fStiffness;
	float m_fDamping;
	float m_fFriction;
	float m_fbBoxSize;
	float m_fBounceBack;
	float m_fSphereSize;

	int m_iAlreadySet;
	int m_iIntegrator;

	bool m_bApplyGravity;
	bool m_bBoundingBox;
	bool m_bAddFriction;

	Vec3 m_vGravity;
	Vec3 m_vLineColor;
	Vec3 m_vSphereScale;

	struct MassPoint
	{
		Vec3 position;
		Vec3 velocity;
		float mass;
		bool isFixed;
		Vec3 force;
		float friction;
	};

	struct Spring
	{
		MassPoint * massPoint1;
		MassPoint * massPoint2;
		float stiffness;
		float initialLength;
		Vec3 currentlength;
	};

	std::vector<MassPoint> m_vlMassPoints;
	std::vector<Spring> m_vlSprings;

	// UI Attributes
	Vec3 m_externalForce;
	Point2D m_mouse;
	Point2D m_trackmouse;
	Point2D m_oldtrackmouse;
};
#endif