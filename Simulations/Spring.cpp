#include "Spring.h"
using namespace GamePhysics;

Spring::Spring(int mp1, int mp2, float initLen, float stiff)
{
	massPoint1 = mp1;
	massPoint2 = mp2;
	initialLength = initLen;
	stiffness = stiff;
}

Spring::~Spring() {}