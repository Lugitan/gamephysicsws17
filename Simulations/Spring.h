#pragma once
#include "util/vectorbase.h"
using namespace GamePhysics;

class Spring
{
	Spring(int massPoint1, int massPoint2, float initialLength, float stiffness);
	~Spring();

	int massPoint1;
	int massPoint2;
	float stiffness;
	float initialLength;
	float currentLength;
};
